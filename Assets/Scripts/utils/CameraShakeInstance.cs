﻿using Cinemachine;
using Singleton;
using UnityEngine;

namespace utils
{
    public class CameraShakeInstance : ResourceSingleton<CameraShakeInstance>
    {
        [SerializeField] private NoiseSettings noiseSettings;
        [SerializeField] private float frequencyGain;

        [Header("TEST")] [SerializeField] private float amplitudeGain;
        [SerializeField] private float duration;

        private CinemachineFreeLook _freeLookCamera;

        private float _intensity;
        private float _time;

        public void Shake(float intensity, float time, float delay = 0)
        {
            _intensity = intensity;
            _time = time;

            if (delay == 0)
            {
                Shake();
                return;
            }

            CancelInvoke(nameof(Shake));
            Invoke(nameof(Shake), delay);
        }

        private void Shake()
        {
            if (!_freeLookCamera)
            {
                _freeLookCamera = FindObjectOfType<CinemachineFreeLook>();
                if (!_freeLookCamera) return;
            }

            for (int i = 0; i < 3; i++)
            {
                var multiChannelPerlin = _freeLookCamera.GetRig(i)
                    .GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                if (!multiChannelPerlin)
                {
                    multiChannelPerlin = _freeLookCamera.GetRig(i)
                        .AddCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                    multiChannelPerlin.m_NoiseProfile = noiseSettings;
                }

                multiChannelPerlin.m_FrequencyGain = frequencyGain;

                LeanTween.cancel(gameObject);
                LeanTween.value(1, 0, _time)
                    .setOnUpdate(v => { multiChannelPerlin.m_AmplitudeGain = _intensity * v; })
                    .setOnComplete(() => { multiChannelPerlin.m_AmplitudeGain = 0; });
            }
        }
    }
}