﻿using System;
using UnityEngine;

namespace utils
{
    public class GameDebug : MonoBehaviour
    {
        [SerializeField] private bool dontDestroyOnLoad;
        [SerializeField] private float fpsUpdateInterval;
        [SerializeField] private Color textColor = Color.white;
        [Range(1, 50)] [SerializeField] private int fontSize;

        private readonly GUIStyle guiStyle = new GUIStyle();

        private float _lastFpsUpdate;
        private int _fps = 30;

        private void Awake()
        {
            SetUpGui();

            if (dontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            UpdateFps();
            QuitGame();
        }

        private void QuitGame()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void OnGUI()
        {
            GUI.Label(new Rect(10, 10, 400, 30), $"v{Application.version}", guiStyle);

            string date = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");
            GUI.Label(new Rect(10, 30, 400, 30), $"{_fps} fps, {date}", guiStyle);
        }

        private void SetUpGui()
        {
            guiStyle.normal.textColor = textColor;
            guiStyle.fontSize = fontSize;
        }

        private void UpdateFps()
        {
            if (Time.realtimeSinceStartup < _lastFpsUpdate) return;
            _lastFpsUpdate = Time.realtimeSinceStartup + fpsUpdateInterval;
            _fps = JExtension.GetFrameRate();
        }
    }
}