﻿using System;
using core.entity.player;
using game.gameplayer;
using game.lobby;
using Mirror;
using networking;
using ui.client;
using UnityEngine;

namespace game
{
    public class GameLobby : NetworkBehaviour
    {
        private const IDisplayNameBgStyle.DisplayNameBgStyle READY_NAMETAGE_STLYE =
            IDisplayNameBgStyle.DisplayNameBgStyle.GREEN;

        private const IDisplayNameBgStyle.DisplayNameBgStyle NOT_READY_NAMETAGE_STLYE =
            IDisplayNameBgStyle.DisplayNameBgStyle.BLUE;

        private const float SEND_COMMAND_DELAY = 0.1f;

        [Header("READY BOARD")] [SerializeField]
        private LobbyPlayerBoard lobbyPlayerBoard;

        private readonly SyncDictionary<GameObject, string> _lobbyPlayers = new SyncDictionary<GameObject, string>();
        public event Action<int, int> OnPlayerCountChanged;
        public event Action<int, int> OnReadyCountChanged;

        public override void OnStartServer()
        {
            base.OnStartServer();
            _lobbyPlayers.Callback += OnLobbyPlayerStateChangedEvent;
            MyNetworkManager.OnPlayerJoined += OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit += OnPlayerQuitEvent;
        }

        public override void OnStopServer()
        {
            base.OnStopServer();

            _lobbyPlayers.Callback -= OnLobbyPlayerStateChangedEvent;
            MyNetworkManager.OnPlayerJoined -= OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit -= OnPlayerQuitEvent;
        }

        public int GetReadyPlayerCount() => _lobbyPlayers.Count;
        public int GetCurrentPlayerCount() => MyNetworkManager.Instance.GetOnlineGamePlayers().Count;
        public int GetMaxPlayerCount() => MyNetworkManager.Instance.maxConnections;

        public int GetMinimumPlayerCount()
        {
            int currentPlayer = GetCurrentPlayerCount();
            return currentPlayer <= 2 ? 2 : Mathf.CeilToInt(currentPlayer / 2f);
        }

        public void ToggleReady(Player player)
        {
            if (_lobbyPlayers.ContainsKey(player.GetIdentity()))
            {
                //Not ready
                _lobbyPlayers.Remove(player.GetIdentity());
            }
            else
            {
                //Ready
                _lobbyPlayers.Add(player.GetIdentity(), player.GetDisplayName());
            }

            CheckGameReady();
        }

        private void OnPlayerJoinedEvent(GamePlayer gamePlayer)
        {
            SendPlayerCount();
            SendReadyCount();
        }

        private void OnPlayerQuitEvent(GamePlayer gamePlayer)
        {
            GameObject connectionId = gamePlayer.ToPlayer().GetIdentity();
            if (!_lobbyPlayers.ContainsKey(connectionId)) return;
            _lobbyPlayers.Remove(connectionId);

            CheckGameReady();
            SendPlayerCount();
            SendReadyCount();
        }

        private void CheckGameReady()
        {
            int readyPlayers = GetReadyPlayerCount();
            if (readyPlayers < GetMinimumPlayerCount()) return;
            MyNetworkManager.Instance.LoadRandomMap();
        }

        private void OnLobbyPlayerStateChangedEvent(SyncIDictionary<GameObject, string>.Operation op,
            GameObject playerObject,
            string displayName)
        {
            Player player = playerObject.GetComponent<Player>();
            if (op == SyncIDictionary<GameObject, string>.Operation.OP_ADD)
            {
                ClientUIManager.SetPlayerNameTagColor(player, READY_NAMETAGE_STLYE);
                RpcUpdateBoard(true, playerObject);
            }

            if (op == SyncIDictionary<GameObject, string>.Operation.OP_REMOVE)
            {
                ClientUIManager.SetPlayerNameTagColor(player, NOT_READY_NAMETAGE_STLYE);
                RpcUpdateBoard(false, playerObject);
            }

            SendReadyCount();
        }


        private void SendPlayerCount()
        {
            CancelInvoke(nameof(SendPlayerDelay));
            Invoke(nameof(SendPlayerDelay), SEND_COMMAND_DELAY);
        }

        private void SendReadyCount()
        {
            CancelInvoke(nameof(SendReadyDelay));
            Invoke(nameof(SendReadyDelay), SEND_COMMAND_DELAY);
        }

        private void SendPlayerDelay()
        {
            RpcUpdatePlayerCount(GetCurrentPlayerCount(), GetMaxPlayerCount());
        }

        private void SendReadyDelay()
        {
            RpcUpdateReadyCount(GetReadyPlayerCount(), GetMinimumPlayerCount());
        }

        [ClientRpc]
        private void RpcUpdatePlayerCount(int currentPlayer, int maxPlayer)
        {
            OnPlayerCountChanged?.Invoke(currentPlayer, maxPlayer);
        }

        [ClientRpc]
        private void RpcUpdateReadyCount(int ready, int minimum)
        {
            OnReadyCountChanged?.Invoke(ready, minimum);
        }

        [ClientRpc]
        private void RpcUpdateBoard(bool isReady, GameObject playerGameObject)
        {
            Player player = playerGameObject.GetComponent<Player>();

            if (isReady)
            {
                lobbyPlayerBoard.Ready(player);
            }
            else
            {
                lobbyPlayerBoard.UnReady(player);
            }
        }
    }
}