﻿using core.interact;
using DG.Tweening;
using scriptsableobject;
using sfx;
using UnityEngine;

namespace game.lobby
{
    public class LobbyReadyButton : InteractableObject
    {
        [SerializeField] private GameLobby gameLobby;

        [Header("BUTTON")] [SerializeField] private Transform buttonTransform;
        [SerializeField] private float pressDuration;
        [SerializeField] private float pressLength;
        [SerializeField] private Ease ease;
        [SerializeField] private EnumValue pressButtonSFX;
        
        public override void Interact()
        {
            gameLobby.ToggleReady(player);

            DOTween.Kill(buttonTransform);
            buttonTransform.transform.localPosition = Vector3.zero;
            buttonTransform.DOLocalMoveY(-pressLength, pressDuration)
                .SetEase(ease)
                .SetLoops(1, LoopType.Restart);
            
            SoundInstance.Instance.PlaySound(pressButtonSFX);
        }
    }
}