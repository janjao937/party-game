﻿using System.Collections.Generic;
using core.entity.player;
using Mirror;
using UnityEngine;

namespace game.lobby
{
    public class LobbyPlayerBoard : NetworkBehaviour
    {
        [SerializeField] private LobbyBoardItem lobbyBoardItemTemplate;
        [SerializeField] private Transform lobbyBoardContainer;
        private readonly Dictionary<Player, LobbyBoardItem> _readyPlayers = new Dictionary<Player, LobbyBoardItem>();

        private void Awake()
        {
            lobbyBoardItemTemplate.gameObject.SetActive(false);
        }

        public void Ready(Player player)
        {
            if (_readyPlayers.ContainsKey(player)) return;
            LobbyBoardItem lobbyBoardItem = Instantiate(lobbyBoardItemTemplate, lobbyBoardContainer);
            lobbyBoardItem.SetDisplayName(player.GetDisplayName());
            lobbyBoardItem.gameObject.SetActive(true);
            _readyPlayers.Add(player, lobbyBoardItem);
        }

        public void UnReady(Player player)
        {
            if (!_readyPlayers.ContainsKey(player)) return;
            LobbyBoardItem lobbyBoardItem = _readyPlayers[player];
            Destroy(lobbyBoardItem.gameObject);
            _readyPlayers.Remove(player);
        }
    }
}