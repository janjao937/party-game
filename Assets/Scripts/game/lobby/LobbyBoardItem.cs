﻿using TMPro;
using UnityEngine;

namespace game.lobby
{
    public class LobbyBoardItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI displayNameText;

        public void SetDisplayName(string displayName)
        {
            displayNameText.text = displayName;
        }
    }
}