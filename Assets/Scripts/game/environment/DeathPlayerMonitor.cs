﻿using System.Collections.Generic;
using DG.Tweening;
using scriptsableobject;
using sfx;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace game.environment
{
    public class DeathPlayerMonitor : MonoBehaviour
    {
        [Header("SFX")] [SerializeField] private bool useSound;
        [SerializeField] private EnumValue announcedSound;
        [Header("MONITOR")] [SerializeField] private bool keepDisplay;
        [SerializeField] private Transform monitorTransform;
        [SerializeField] private TextMeshProUGUI victimText;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private float fadeLength;
        [SerializeField] private float stayLength;
        [SerializeField] private float inLength;
        [SerializeField] private float outLength;
        [SerializeField] private Ease inEase;
        [SerializeField] private Ease outEase;

        private readonly Queue<string> _victims = new Queue<string>();
        private bool _isAnnouncing;

        public void Announced(string victim)
        {
            if (_isAnnouncing)
            {
                _victims.Enqueue(victim);
                return;
            }

            _isAnnouncing = true;
            victimText.text = victim;

            canvasGroup.alpha = 0;
            Vector3 tinyScale = Vector3.one * 0.0001f;
            monitorTransform.localScale = tinyScale;
            monitorTransform.DOScale(Vector3.one, inLength)
                .SetEase(inEase)
                .OnComplete(() =>
                {
                    if (useSound)
                    {
                        SoundInstance.Instance.PlaySound(announcedSound);
                    }

                    canvasGroup.DOFade(1, fadeLength)
                        .OnComplete(() =>
                        {
                            monitorTransform.DOScale(Vector3.one, stayLength).OnComplete(() =>
                            {
                                monitorTransform.DOScale(keepDisplay ? Vector3.one : tinyScale, outLength)
                                    .SetEase(outEase).OnComplete(() =>
                                    {
                                        _isAnnouncing = false;
                                        if (_victims.Count > 0)
                                        {
                                            Announced(_victims.Dequeue());
                                        }
                                    });
                            });
                        });
                });
        }
    }
}