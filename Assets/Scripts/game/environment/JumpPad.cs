﻿using core.entity;
using core.entity.player;
using scriptsableobject;
using sfx;
using UnityEngine;

namespace game.environment
{
    public class JumpPad : MonoBehaviour
    {
        [SerializeField] private EnumValue bounceSound;
        [SerializeField] private float bouncingForce;
        [SerializeField] private Vector3 bouncingDirection;
        [SerializeField] private float damage;

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.transform.TryGetComponent(out PlayerMovement playerMovement)) return;

            Vector3 force = bouncingDirection;

            if (bouncingDirection.magnitude == 0)
            {
                Vector3 dir = (collision.transform.position - collision.contacts[0].point).normalized;
                dir.y = 0;
                force = Vector3.up + dir;
            }

            if (damage > 0 && collision.transform.TryGetComponent(out IDamageable damageable))
            {
                damageable.Damage(damage, DeathEffect.RAGDOLL);
            }

            playerMovement.rb.AddForce(force * bouncingForce, ForceMode.Impulse);
            SoundInstance.Instance.PlaySound(bounceSound, collision.transform.position);
        }
    }
}