﻿using UnityEngine;

namespace game.environment.syncobstacle
{
    public abstract class BaseSyncObstacle : MonoBehaviour
    {
        private const int AUTO_SYNCE_DELAY = 7;
        public bool IsSync { get; private set; }

        protected virtual void Start()
        {
            Invoke(nameof(StartSync), AUTO_SYNCE_DELAY);
        }

        public void StartSync()
        {
            if (IsSync) return;
            IsSync = true;

            Sync();
        }

        protected abstract void Sync();
    }
}