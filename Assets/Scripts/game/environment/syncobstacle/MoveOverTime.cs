﻿using DG.Tweening;
using UnityEngine;

namespace game.environment.syncobstacle
{
    public class MoveOverTime : BaseSyncObstacle
    {
        [SerializeField] private bool loop = true;
        [SerializeField] private float startDelay;
        [SerializeField] private float duration;
        [SerializeField] private float moveLength;
        [SerializeField] private Vector3 moveWorldDirection;
        [SerializeField] private Ease ease = Ease.Linear;

        protected override void Sync()
        {
            Invoke(nameof(DoMove), startDelay);
        }

        private void DoMove()
        {
            if (loop)
            {
                transform
                    .DOMove(transform.position + moveWorldDirection * moveLength, duration)
                    .SetEase(ease)
                    .SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                transform
                    .DOMove(transform.position + moveWorldDirection * moveLength, duration)
                    .SetEase(ease);
            }
        }
    }
}