﻿using System;
using assets;

namespace core.inventory.item
{
    public struct ItemStack
    {
        public Material material;
        public string itemName;

        public static ItemStack CreateItem(Material material)
        {
            ItemAssetSO.ItemAsset itemAsset = GameAssetInstance.Instance.ItemAssets.GetItemAsset(material);
            return new ItemStack
            {
                material = material,
                itemName = itemAsset.itemName,
            };
        }
    }
}