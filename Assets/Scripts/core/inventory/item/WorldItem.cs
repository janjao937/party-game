﻿using core.entity.player;
using core.interact;
using UnityEngine;

namespace core.inventory.item
{
    public abstract class WorldItem : MonoBehaviour, IInteractable
    {
        protected ClickActionType clickActionType;
        protected Player player;

        public void Init(Player player, ClickActionType clickActionType)
        {
            this.player = player;
            this.clickActionType = clickActionType;
        }

        public abstract void Interact();
    }
}