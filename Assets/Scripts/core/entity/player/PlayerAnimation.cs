﻿using core.interact;
using Mirror;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerAnimation : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        private PlayerMovement _playerMovement;
        private PlayerInteract _playerInteract;
        private NetworkAnimator _networkAnimator;
        private Player _player;

        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _playerInteract = GetComponent<PlayerInteract>();
            _networkAnimator = GetComponent<NetworkAnimator>();
            _player = GetComponent<Player>();
        }

        private void OnEnable()
        {
            _playerMovement.OnJump += OnJumpEvent;
            _playerMovement.OnDash += OnDashEvent;
            _playerMovement.OnKnockout += OnKnockoutEvent;
            _playerMovement.OnRunToStop += OnRunToStopEvent;
            _playerMovement.OnSprintToggle += OnSprintToggleEvent;
            _playerMovement.OnGroundedChanged += OnGroundedChangedEvent;

            _playerInteract.OnPlayerUseItem += OnPlayerUseItemEvent;
        }

        private void OnDisable()
        {
            _playerMovement.OnJump -= OnJumpEvent;
            _playerMovement.OnDash -= OnDashEvent;
            _playerMovement.OnKnockout -= OnKnockoutEvent;
            _playerMovement.OnRunToStop -= OnRunToStopEvent;
            _playerMovement.OnSprintToggle -= OnSprintToggleEvent;
            _playerMovement.OnGroundedChanged -= OnGroundedChangedEvent;

            _playerInteract.OnPlayerUseItem -= OnPlayerUseItemEvent;
        }

        private void FixedUpdate()
        {
            animator.SetFloat("Vertical", _playerMovement.vertical);
            animator.SetFloat("Horizontal", _playerMovement.horizontal);
        }

        private void OnPlayerUseItemEvent(Player who, ClickActionType clickActionType)
        {
            if (who != _player) return;
            if (clickActionType != ClickActionType.DOWN) return;
            _networkAnimator.SetTrigger("Punch");
        }

        private void OnRunToStopEvent()
        {
            _networkAnimator.SetTrigger("RunToStop");
        }

        private void OnJumpEvent()
        {
            _networkAnimator.SetTrigger("Jump");
        }

        private void OnDashEvent()
        {
        }

        private void OnKnockoutEvent()
        {
            animator.SetBool("Grounded", true);
            _networkAnimator.SetTrigger("KnockOut");
        }

        private void OnSprintToggleEvent(bool sprint)
        {
            animator.SetBool("Sprint", sprint);
        }

        private void OnGroundedChangedEvent(bool grounded)
        {
            animator.SetBool("Grounded", grounded);
        }
    }
}