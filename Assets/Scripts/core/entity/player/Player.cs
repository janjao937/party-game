﻿using System;
using core.entity.player.gamemode;
using Mirror;
using Steamworks;
using UnityEngine;

namespace core.entity.player
{
    public class Player : HumanEntity, IPlayer, INetworkPlayer
    {
        [Header("PLAYER")] [SerializeField] private Transform playerHeadTransform;

        private PlayerMovement _playerMovement;
        private int _connectionId;

        [SyncVar(hook = nameof(OnPlayerVisibleChangedEvent))]
        private bool _visible = true;

        [SyncVar(hook = nameof(OnPlayerColliderChangedEvent))]
        private bool _collider = true;

        [SyncVar(hook = nameof(OnPlayerGameModeChangedEvent))]
        private GameMode _gameMode = GameMode.SURVIVAL;

        [SyncVar(hook = nameof(OnPlayerDisplayNameChangedEvent))]
        private string _displayName;

        [SyncVar(hook = nameof(OnPlayerInteractableChangedEvent))]
        private bool _interactable = true;

        [SyncVar(hook = nameof(OnPlayerFreezeEvent))]
        private bool _freeze;

        //Event
        public static event Action<Player, string> OnPlayerChat;
        public event Action<Player, string> OnPlayerDisplayNameChanged;
        public event Action<Player, bool> OnPlayerVisibleChanged;
        public event Action<Player, bool> OnPlayerColliderChanged;
        public event Action<Player, GameMode> OnPlayerGameModeChanged;
        public event Action<Player, bool> OnPlayerFreeze;
        public event Action<Player, bool> OnPlayerInteractableChanged;

        protected override void Awake()
        {
            base.Awake();

            _playerMovement = GetComponent<PlayerMovement>();
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();

#if !UNITY_EDITOR
            UpdateLocalDisplayName();
#endif
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            _connectionId = netIdentity.connectionToClient.connectionId;
        }

        #region DEATH

        protected override void OnDeath(DeathEffect deathEffect)
        {
            base.OnDeath(deathEffect);

            SetVisible(false);
            SetCollider(false);
            _playerMovement.SetWalkSpeed(0f);
            Invoke(nameof(DeathDelay), 5f);
        }

        private void DeathDelay()
        {
            SetGameMode(GameMode.SPECTATOR);
            _playerMovement.SetWalkSpeed(1f);
        }

        #endregion

        public void AddVelocity(Vector3 velocity) => TargetAddVelocity(velocity);
        public void SetVisible(bool visible) => _visible = visible;
        public bool IsVisible() => _visible;

        public void SetGameMode(GameMode gameMode)
        {
            SetVisible(gameMode == GameMode.SURVIVAL);
            SetCollider(gameMode == GameMode.SURVIVAL);
            _gameMode = gameMode;
        }

        public GameMode GetGameMode() => _gameMode;
        public void SetFreeze(bool freeze) => _freeze = freeze;
        public bool IsFreeze() => _freeze;
        public void SetInteractable(bool isAllow) => _interactable = isAllow;
        public bool IsInteractAble() => _interactable;
        public void SetWalkSpeed(float speed) => _playerMovement.SetWalkSpeed(speed);
        public float GetWalkSpeed() => _playerMovement.WalkSpeed;
        public Transform GetHeadTransform() => playerHeadTransform;
        public void SetCollider(bool value) => _collider = value;
        public string GetDisplayName() => _displayName;
        public void SetDisplayName(string value) => _displayName = value;
        public void SendChatMessage(string value) => CmdSendMessage(value);

        public void SendLocalChatMessage(string value)
        {
            NetworkIdentity identity = gameObject.GetComponent<NetworkIdentity>();
            TargetSendLocalMessage(identity.connectionToClient, value);
        }

        #region DISPLAYNAME

        private void UpdateLocalDisplayName()
        {
            string displayName = SteamManager.Initialized ? SteamFriends.GetPersonaName() : name;
            CmdUpdateSteamName(displayName);
        }

        [Command]
        private void CmdUpdateSteamName(string steamName)
        {
            SetDisplayName(steamName);
        }

        #endregion

        #region CHAT

        [Command]
        private void CmdSendMessage(string value)
        {
            RpcSendMessage(value);
        }

        [ClientRpc]
        private void RpcSendMessage(string value)
        {
            OnPlayerChat?.Invoke(this, value);
        }

        [TargetRpc]
        private void TargetSendLocalMessage(NetworkConnection conn, string value)
        {
            OnPlayerChat?.Invoke(this, value);
        }

        #endregion

        #region ADD VELOCITY

        [TargetRpc]
        private void TargetAddVelocity(Vector3 velocity)
        {
            _playerMovement.rb.AddForce(velocity, ForceMode.Impulse);
        }

        #endregion

        #region NETWORK PLAYER

        public int GetConnectionId() => _connectionId;
        public void SetConnectionId(int connectionId) => _connectionId = connectionId;

        #endregion


        #region PLAYER EVENT

        private void OnPlayerDisplayNameChangedEvent(string old, string newValue) =>
            OnPlayerDisplayNameChanged?.Invoke(this, newValue);

        private void OnPlayerVisibleChangedEvent(bool old, bool newValue) =>
            OnPlayerVisibleChanged?.Invoke(this, newValue);

        private void OnPlayerColliderChangedEvent(bool old, bool newValue) =>
            OnPlayerColliderChanged?.Invoke(this, newValue);

        private void OnPlayerGameModeChangedEvent(GameMode old, GameMode newValue) =>
            OnPlayerGameModeChanged?.Invoke(this, newValue);

        private void OnPlayerFreezeEvent(bool old, bool newValue) =>
            OnPlayerFreeze?.Invoke(this, newValue);

        private void OnPlayerInteractableChangedEvent(bool old, bool newValue) =>
            OnPlayerInteractableChanged?.Invoke(this, newValue);

        #endregion

        public override string ToString()
        {
            return GetDisplayName();
        }
    }
}