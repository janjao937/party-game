﻿using System;
using core.entity.player.gamemode;
using Mirror;
using networking;
using Steamworks;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerVoiceChat : NetworkBehaviour
    {
        public enum VoiceChatType
        {
            PUSH_TO_TALK,
            ACTIVATION,
        }

        public event Action<bool> OnPushToTalk;
        public event Action<VoiceChatType> OnVoiceChatTypeChanged;

        [SerializeField] private float playerRadius = 50f;

        public float PlayerRadius => playerRadius;
        private VoiceChatType eVoiceChatType;

        private bool _isVoiceRecording;
        private Player _player;

        public VoiceChatType EVoiceChatType
        {
            get => eVoiceChatType;

            set
            {
                if (eVoiceChatType != value)
                {
                    if (value == VoiceChatType.ACTIVATION)
                    {
                        _isVoiceRecording = true;
                        if (SteamManager.Initialized) ;
                        SteamUser.StartVoiceRecording();
                    }
                    else
                    {
                        _isVoiceRecording = false;
                        SteamUser.StopVoiceRecording();
                    }
                }

                eVoiceChatType = value;
                OnVoiceChatTypeChanged?.Invoke(value);
            }
        }

        private void Awake()
        {
            _player = GetComponent<Player>();
        }

        private void OnEnable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;
        }

        private void OnDisable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;

            if (_isVoiceRecording)
            {
                SteamUser.StopVoiceRecording();
            }
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            if (EVoiceChatType == VoiceChatType.ACTIVATION) return;
            switch (inputAction)
            {
                case PlayerInput.EInputAction.V_KEY_DOWN:
                    _isVoiceRecording = true;
                    if (!CanVoiceChat()) return;
                    if (SteamManager.Initialized) ;
                    SteamUser.StartVoiceRecording();
                    OnPushToTalk?.Invoke(true);
                    break;
                case PlayerInput.EInputAction.V_KEY_UP:
                    _isVoiceRecording = false;
                    if (!CanVoiceChat()) return;
                    SteamUser.StopVoiceRecording();
                    OnPushToTalk?.Invoke(false);
                    break;
            }
        }

        private bool CanVoiceChat()
        {
            return !_player.IsDead() && _player.GetGameMode() != GameMode.SPECTATOR;
        }

        void Update()
        {
            if (!CanVoiceChat()) return;
            if (!_isVoiceRecording) return;
            EVoiceResult voiceResult = SteamUser.GetAvailableVoice(out uint compressed);
            if (voiceResult != EVoiceResult.k_EVoiceResultOK || compressed <= 1024) return;
            //Debug.Log($"[VoiceChat] compressed: {compressed}");
            byte[] destBuffer = new byte[1024];
            voiceResult = SteamUser.GetVoice(true, destBuffer, 1024, out uint bytesWritten);
            if (voiceResult == EVoiceResult.k_EVoiceResultOK && bytesWritten > 0)
            {
                CmdSendVoiceData(gameObject, destBuffer, bytesWritten);
            }
        }

        [Command(channel = 2)]
        void CmdSendVoiceData(GameObject speaker, byte[] data, uint size)
        {
            foreach (var gamePlayer in MyNetworkManager.Instance.GetOnlineGamePlayers().Values)
            {
                float distToListener =
                    Vector3.Distance(gamePlayer.playerGameObject.transform.position, transform.position);
                if (distToListener > playerRadius) continue;
                NetworkIdentity networkIdentity = gamePlayer.playerGameObject.GetComponent<NetworkIdentity>();
                if (!networkIdentity) return;
                TargetReceiveVoiceChat(networkIdentity.connectionToClient, speaker, data, size);
            }
        }

        [TargetRpc(channel = 2)]
        void TargetReceiveVoiceChat(NetworkConnection connection, GameObject playerGameObject, byte[] destBuffer,
            uint bytesWritten)
        {
            byte[] destBuffer2 = new byte[22050 * 2];
            uint bytesWritten2;
            EVoiceResult ret = SteamUser.DecompressVoice(destBuffer, bytesWritten, destBuffer2,
                (uint)destBuffer2.Length, out bytesWritten2, 22050);
            if (ret == EVoiceResult.k_EVoiceResultOK && bytesWritten2 > 0)
            {
                AudioClip clip = AudioClip.Create(UnityEngine.Random.Range(100, 1000000).ToString(),
                    22050, 1, 22050, false);

                float[] test = new float[22050];
                for (int i = 0; i < test.Length; ++i)
                {
                    var sample = (short)(destBuffer2[i * 2] | destBuffer2[i * 2 + 1] << 8) / 32768.0f;
                    test[i] = sample;
                }

                clip.SetData(test, 0);
                playerGameObject.GetComponent<PlayerVoiceChatHandler>().PlayVoiceClip(clip);
            }
        }
    }
}