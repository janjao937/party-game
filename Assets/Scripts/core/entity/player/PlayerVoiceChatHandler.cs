﻿using System;
using Mirror;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace core.entity.player
{
    [RequireComponent(typeof(PlayerVoiceChat))]
    public class PlayerVoiceChatHandler : NetworkBehaviour
    {
        [SerializeField] private bool hearOwnVoice;

        [Header("AUDIO SOURCE")] [Range(0, 1f)] [SerializeField]
        private float distortionLevel = 0.7f;

        [Range(0, 1f)] [SerializeField] private float spatialBlend = 1f;
        [SerializeField] private AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear;

        public bool IsVoiceChatEnable() => _voiceVolume > 0f;
        private float _voiceVolume = 1f;

        private AudioSource _audioSource;
        private AudioDistortionFilter _distortionFilter;
        private PlayerVoiceChat _playerVoiceChat;
        public event Action<float> OnVoiceLevelChanged;

        public float VoiceVolume
        {
            get => _voiceVolume;
            set
            {
                _voiceVolume = Mathf.Clamp01(value);
                _distortionFilter.distortionLevel = distortionLevel * _voiceVolume;
                _playerVoiceChat.enabled = IsVoiceChatEnable();
            }
        }

        private void Awake()
        {
            _playerVoiceChat = GetComponent<PlayerVoiceChat>();

            InitAudioSource();
        }

        #region VOICE LISTENER

        private void InitAudioSource()
        {
            if (!GetComponent<AudioSource>())
            {
                _audioSource = transform.AddComponent<AudioSource>();
            }

            if (!GetComponent<AudioDistortionFilter>())
            {
                _distortionFilter = transform.AddComponent<AudioDistortionFilter>();
            }

            _distortionFilter.distortionLevel = distortionLevel;
            _audioSource.playOnAwake = false;
            _audioSource.spatialBlend = spatialBlend;
            _audioSource.maxDistance = GetComponent<PlayerVoiceChat>().PlayerRadius;
            _audioSource.rolloffMode = audioRolloffMode;
        }

        #endregion

        public void PlayVoiceClip(AudioClip audioClip)
        {
            OnVoiceLevelChanged?.Invoke(Random.Range(0.5f, 1f));

            if (isLocalPlayer && !hearOwnVoice)
            {
                return;
            }

            _audioSource.clip = audioClip;
            _audioSource.Play();
            
        }
    }
}