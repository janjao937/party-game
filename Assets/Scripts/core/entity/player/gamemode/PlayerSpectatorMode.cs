﻿using System;
using System.Linq;
using Mirror;
using UnityEngine;

namespace core.entity.player.gamemode
{
    public class PlayerSpectatorMode : NetworkBehaviour
    {
        private enum SpectatorState
        {
            FREE_LOOK,
            FOLLOW_TARGET
        }

        [SerializeField] private float moveSpeed;
        [SerializeField] private float sensitivity;
        [SerializeField] private Camera playerCamera;

        private Vector3 _moveDirection;

        private float _vertical;
        private float _horizontal;

        private float _mouseX;
        private float _mouseY;

        private float rotateX;

        private bool _isInputRegistered;
        private SpectatorState _spectatorState;
        private int _currentSpectatorIndex;

        private PlayerComponentEnable _playerComponentEnable;

        private void Awake()
        {
            _playerComponentEnable = GetComponent<PlayerComponentEnable>();
        }

        private void Start()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            if (isLocalPlayer)
            {
                playerCamera.tag = "MainCamera";
            }
        }

        private void RegisterInput()
        {
            if (_isInputRegistered) return;
            _isInputRegistered = true;

            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;
            playerInput.OnInputValueUpdated += OnInputValueUpdatedEvent;
            playerInput.OnMouseValueUpdated += OnMouseValueUpdatedEvent;
        }

        private void UnregisterInput()
        {
            if (!_isInputRegistered) return;
            _isInputRegistered = false;

            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;
            playerInput.OnInputValueUpdated -= OnInputValueUpdatedEvent;
            playerInput.OnMouseValueUpdated -= OnMouseValueUpdatedEvent;
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.LEFT_CLICK_KEY_DOWN:
                    FindPlayerToFollow(true);
                    break;
                case PlayerInput.EInputAction.RIGHT_CLICK_KEY_DOWN:
                    FindPlayerToFollow(false);
                    break;
                case PlayerInput.EInputAction.JUMP_KEY_DOWN:
                    _moveDirection = Vector3.up;
                    break;
                case PlayerInput.EInputAction.JUMP_KEY_UP:
                    _moveDirection = Vector3.zero;
                    break;
                case PlayerInput.EInputAction.SHIFT_KEY_DOWN:
                    _moveDirection = Vector3.down;
                    break;
                case PlayerInput.EInputAction.SHIFT_KEY_UP:
                    _moveDirection = Vector3.zero;
                    break;
            }
        }

        public void FindPlayerToFollow(bool nextPlayer)
        {
            Player[] otherPlayers = FindObjectsOfType<Player>().Where(p =>
                    p.GetGameMode() != GameMode.SPECTATOR &&
                    p.gameObject != NetworkClient.localPlayer.gameObject)
                .ToArray();

            if (otherPlayers.Length == 0)
            {
                Debug.Log("No player to follow!");
                return;
            }

            Player playerToFollow;

            if (nextPlayer)
            {
                playerToFollow = otherPlayers[_currentSpectatorIndex++ % otherPlayers.Length];
            }
            else
            {
                if (--_currentSpectatorIndex < 0 || _currentSpectatorIndex >= otherPlayers.Length)
                {
                    _currentSpectatorIndex = otherPlayers.Length - 1;
                }

                playerToFollow = otherPlayers[_currentSpectatorIndex];
            }

            SetFollow(playerToFollow != null ? playerToFollow : null);
        }

        private void OnInputValueUpdatedEvent(PlayerInput.EInputValue inputValue, float value)
        {
            if (_spectatorState == SpectatorState.FOLLOW_TARGET && Math.Abs(value) >= 0.2f)
            {
                SetFollow(null);
                return;
            }

            Move(_vertical = inputValue == PlayerInput.EInputValue.VERTICAL ? value : _vertical,
                _horizontal = inputValue == PlayerInput.EInputValue.HORIZONTAL ? value : _horizontal);
        }

        private void OnMouseValueUpdatedEvent(PlayerInput.EMouseValue mouseValue, float value)
        {
            Look(_mouseX = mouseValue == PlayerInput.EMouseValue.PITCH ? value : _mouseX,
                _mouseY = mouseValue == PlayerInput.EMouseValue.YAW ? value : _mouseY);
        }

        private void Move(float v, float h)
        {
            if (_spectatorState != SpectatorState.FREE_LOOK) return;
            Vector3 moveDir = playerCamera.transform.forward * v + playerCamera.transform.right * h;
            moveDir += _moveDirection;
            moveDir.Normalize();

            transform.position += moveDir * moveSpeed * Time.deltaTime;
        }

        private void Look(float x, float y)
        {
            if (_spectatorState != SpectatorState.FREE_LOOK) return;
            x *= sensitivity * Time.deltaTime;
            y *= sensitivity * Time.deltaTime;
            rotateX -= y;
            rotateX = Mathf.Clamp(rotateX, -90, 90);
            playerCamera.transform.localRotation = Quaternion.Euler(rotateX, 0, 0);
            transform.Rotate(Vector3.up * x);
        }

        public void SetSpectator(bool value)
        {
            _playerComponentEnable.SetPlayerInteractEnable(!value);
            _playerComponentEnable.SetPlayerMovementEnable(!value);
            _playerComponentEnable.SetVirtualCameraEnable(!value);

            if (value)
            {
                RegisterInput();
                _spectatorState = SpectatorState.FREE_LOOK;
            }
            else
            {
                _playerComponentEnable.ResetFollow();
                _playerComponentEnable.ResetLookAt();
                playerCamera.transform.localPosition = Vector3.zero;
                playerCamera.transform.localRotation = Quaternion.identity;

                UnregisterInput();
            }
        }

        private void SetFollow(Player player)
        {
            _playerComponentEnable.SetVirtualCameraEnable(player);

            if (player)
            {
                _spectatorState = SpectatorState.FOLLOW_TARGET;

                _playerComponentEnable.SetCameraFollow(player.transform);
                _playerComponentEnable.SetCameraLootAt(player.GetHeadTransform());
            }
            else
            {
                _spectatorState = SpectatorState.FREE_LOOK;
            }
        }
    }
}