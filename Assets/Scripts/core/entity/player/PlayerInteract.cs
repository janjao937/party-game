﻿using System;
using core.interact;
using Mirror;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerInteract : NetworkBehaviour
    {
        [Header("INTERACT")] [SerializeField] private float interactCooldown;

        private float _lastInteract;
        private Player _player;

        //Event
        public event Action<Player, ClickActionType> OnPlayerUseItem;
        public event Action<Player, ClickActionType> OnPlayerInteracted;

        private void Awake()
        {
            _player = GetComponent<Player>();
        }

        private void OnEnable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;
        }

        private void OnDisable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.LEFT_CLICK_KEY_DOWN:
                    UseItem(ClickActionType.DOWN);
                    break;
                case PlayerInput.EInputAction.LEFT_CLICK_KEY_UP:
                    UseItem(ClickActionType.UP);
                    break;
                case PlayerInput.EInputAction.E_KEY_DOWN:
                    Interact(ClickActionType.DOWN);
                    break;
                case PlayerInput.EInputAction.E_KEY_UP:
                    Interact(ClickActionType.UP);
                    break;
            }
        }

        private bool IsInteractCooldown()
        {
            if (Time.time < _lastInteract) return true;
            _lastInteract = Time.time + interactCooldown;
            return false;
        }

        private void UseItem(ClickActionType clickActionType)
        {
            if (IsInteractCooldown()) return;
            CmdPlayerUseItem(_player.GetIdentity(), clickActionType);
        }

        private void Interact(ClickActionType clickActionType)
        {
            if (IsInteractCooldown()) return;
            CmdPlayerInteracted(_player.GetIdentity(), clickActionType);
        }

        #region INTERCT

        [Command]
        private void CmdPlayerUseItem(GameObject who, ClickActionType clickActionType)
        {
            OnPlayerUseItem?.Invoke(who.GetComponent<Player>(), clickActionType);
            RpcPlayerUseItem(who, clickActionType);
        }

        [ClientRpc]
        private void RpcPlayerUseItem(GameObject who, ClickActionType clickActionType)
        {
            //Art Only
            if (isServer) return;
            OnPlayerUseItem?.Invoke(who.GetComponent<Player>(), clickActionType);
        }

        #endregion

        [Command]
        private void CmdPlayerInteracted(GameObject who, ClickActionType clickActionType)
        {
            OnPlayerInteracted?.Invoke(who.GetComponent<Player>(), clickActionType);
            RpcPlayerInteracted(who, clickActionType);
        }

        [ClientRpc]
        private void RpcPlayerInteracted(GameObject who, ClickActionType clickActionType)
        {
            //Art Only
            if (isServer) return;
            OnPlayerInteracted?.Invoke(who.GetComponent<Player>(), clickActionType);
        }
    }
}