﻿using UnityEngine;

namespace core.entity.player.virsual
{
    public class PlayerVision : MonoBehaviour
    {
        [SerializeField] private Transform lookAt;
        [SerializeField] private Transform[] eyes;
        [SerializeField] private float lookSpeed;
        [SerializeField] private float lookMaxAngle= 90;

        private Transform _lookTransform;
        private void FixedUpdate()
        {
            if (!_lookTransform) return;
            if (Vector3.Angle(transform.position, _lookTransform.position) > lookMaxAngle)
            {
                _lookTransform = lookAt;
                return;
            }
            
            foreach (var eye in eyes)
            {
                Vector3 lookDir = (_lookTransform.position - eye.position).normalized;
                lookDir.y = 0;
                Quaternion lookRotation = Quaternion.LookRotation(lookDir);
                eye.rotation = Quaternion.Slerp(eye.rotation, lookRotation, lookSpeed);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            _lookTransform = other.transform;
        }

        private void OnTriggerExit(Collider other)
        {
            _lookTransform = lookAt;
            ResetEyeRotation();
        }

        private void ResetEyeRotation()
        {
            foreach (var eye in eyes)
            {
                eye.localRotation = Quaternion.identity;
            }
        }
    }
}