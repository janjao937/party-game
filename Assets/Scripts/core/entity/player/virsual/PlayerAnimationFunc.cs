﻿using fx;
using scriptsableobject;
using sfx;
using UnityEngine;

namespace core.entity.player.virsual
{
    public class PlayerAnimationFunc : MonoBehaviour
    {
        
        [Header("SFX")]
        [SerializeField] private EnumValue footStepSFX;
        
        [Header("VFX")]
        [SerializeField] private Transform footOffset;
        [SerializeField] private EnumValue footStepFX;
        
        public void OnWalking()
        {
            SoundInstance.Instance.PlaySound(footStepSFX, transform.position);
        }
        
        public void OnRunning()
        {
            SoundInstance.Instance.PlaySound(footStepSFX, transform.position);
            FXInstance.PlayEffect(footStepFX, footOffset.position, Quaternion.identity);
        }
    }
}