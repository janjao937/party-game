// Some stupid rigidbody based movement by jayymmd
// movement ref from Super Mario 64

using System;
using Mirror;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerMovement : NetworkBehaviour
    {
        public enum EJumpLevel
        {
            LEVEL1,
            LEVEL2,
            LEVEL3,
        }

        [Header("VISUAL ONLY")] [SerializeField]
        private Transform modelTransform;

        [SerializeField] private float modelRotateSpeed;
        [Header("DRAG")] [SerializeField] private float airDrag;
        [SerializeField] private float groundDrag;

        [Header("MOVEMENT")] [SerializeField] private float moveSpeed;
        [SyncVar] [SerializeField] private float walkSpeedMultiplier;
        [SerializeField] private float sprintSpeedMultiplier;
        [SerializeField] private float slopeSpeedMultiplier;
        [SerializeField] private float airSpeedMultiplier;
        [SerializeField] private float wallSpeedMultiplier;
        [Range(0, 1f)] [SerializeField] private float moveBackwardFactor;
        [Range(0, 1f)] [SerializeField] private float stopMoveSensitivity;
        [Header("TURN")] [SerializeField] private float turnSpeed;
        [Header("JUMP")] [SerializeField] private float jumpForce;

        [Header("JUMP LEVEL MULTIPLIER")] [SerializeField]
        private float jumpLevelResetDelay;

        [SerializeField] private float level1Multiplier;
        [SerializeField] private float level2Multiplier;
        [SerializeField] private float level3Multiplier;

        [Header("SMASH")] [SerializeField] private bool useSmash;
        [SerializeField] private float smashForce;
        [SerializeField] private float smashForceMultiplier;
        [Header("DASH")] [SerializeField] private bool useDash;
        [SerializeField] private float dashDelay;
        [SerializeField] private float dashForceMultiplier;
        [SerializeField] private float airDashForceMultiplier;
        [SerializeField] private Vector3 airDashDirection;
        [SerializeField] private Vector3 groundDashDirection;
        [Header("KNOCKOUT")] [SerializeField] private bool useKnockout;
        [SerializeField] private float knockOutDuration;
        [SerializeField] private float knockOutForce;
        [SerializeField] private float knockBackForceMultiplier;
        [SerializeField] private float knockOutCheckDistance;
        [SerializeField] private float knockOutDrag;

        [Header("GROUND")] [SerializeField] private float floorAngle;
        [SerializeField] private LayerMask whatIsGround;
        [SerializeField] private float groundCheck;

        [Header("DEBUG")] public float horizontal;
        public float vertical;
        public bool grounded;
        public bool isSprinting;
        public bool canDashing;

        [HideInInspector] public Rigidbody rb;
        private RaycastHit _hit;

        private Transform _cam;
        private Vector3 _forwardDirection;

        public void SetWalkSpeed(float speed)
        {
            walkSpeedMultiplier = speed;
        }

        public float WalkSpeed => walkSpeedMultiplier;

        #region EVENT

        public event Action OnJump;
        public event Action<bool> OnSprintToggle;
        public event Action<bool> OnGroundedChanged;
        public event Action OnDash;
        public event Action OnKnockout;
        public event Action OnRunToStop;

        #endregion

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
            rb.freezeRotation = true;
            rb.useGravity = true;
        }

        private void OnEnable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;
            playerInput.OnInputValueUpdated += OnInputValueUpdatedEvent;
        }

        private void OnDisable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;
            playerInput.OnInputValueUpdated -= OnInputValueUpdatedEvent;
        }

        private void OnInputValueUpdatedEvent(PlayerInput.EInputValue inputValue, float value)
        {
            switch (inputValue)
            {
                case PlayerInput.EInputValue.VERTICAL:
                    vertical = value;
                    break;
                case PlayerInput.EInputValue.HORIZONTAL:
                    horizontal = value;
                    break;
            }
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.C_KEY_DOWN:
                    Dash();
                    break;
                case PlayerInput.EInputAction.JUMP_KEY_DOWN:
                    Jump();
                    break;
                case PlayerInput.EInputAction.RIGHT_CLICK_KEY_DOWN:
                    _forceFirstLevelJump = false;
                    break;
                case PlayerInput.EInputAction.RIGHT_CLICK_KEY_UP:
                    _forceFirstLevelJump = true;
                    break;
                case PlayerInput.EInputAction.SHIFT_KEY_DOWN:
                    isSprinting = true;
                    _isSprintPressed = true;
                    OnSprintToggle?.Invoke(true);
                    break;
                case PlayerInput.EInputAction.SHIFT_KEY_UP:
                    _isSprintPressed = false;
                    break;
                case PlayerInput.EInputAction.CTRL_KEY_DOWN:
                    Smash();
                    break;
            }
        }

        private void Update()
        {
            //Extra gravity
            rb.AddForce(Vector3.down * Time.deltaTime * 10);
        }

        private void FixedUpdate()
        {
            UpdateForwardDirection();

            CheckGround();
            DragControl();
            Movement();
            LookAtVelocityDirection();
            StopMovement();
            CheckCanDash();
            CheckKnockOut();
            CheckRunToStop();
            CheckAutoToggleSprint();
            UpdateModelRotation();
        }


        #region ROTATION

        private void UpdateModelRotation()
        {
            Vector3 lookDirection = rb.velocity;
            lookDirection.y = 0f;

            if (vertical == 0f || lookDirection == Vector3.zero) lookDirection = _forwardDirection;
            else if (!grounded) lookDirection = (lookDirection - _forwardDirection).normalized;

            Quaternion lookRotation = Quaternion.LookRotation(lookDirection);
            modelTransform.transform.rotation =
                Quaternion.Slerp(modelTransform.transform.rotation, lookRotation, modelRotateSpeed);
        }

        private void LookAtVelocityDirection()
        {
            Quaternion lookRotation = Quaternion.LookRotation(_forwardDirection);
            rb.rotation = Quaternion.Slerp(rb.rotation, lookRotation, turnSpeed);
        }

        #endregion

        #region MOVEMENT

        private bool _isRunToStop;

        private void CheckRunToStop()
        {
            if (isKnockOut || !grounded || !isSprinting) return;

            if (vertical > 0)
            {
                _isRunToStop = false;
                return;
            }

            if (_isRunToStop) return;
            _isRunToStop = true;
            OnRunToStop?.Invoke();
        }

        private void StopMovement()
        {
            if (rb.isKinematic) return;
            if (!(vertical == 0 && grounded)) return;
            rb.velocity *= stopMoveSensitivity;
        }

        private bool _isSprintPressed;
        private void CheckAutoToggleSprint()
        {
            if (!isSprinting || MathF.Abs(vertical) > 0f || _isSprintPressed) return;
            isSprinting = false;
            OnSprintToggle?.Invoke(false);
        }

        private void Movement()
        {
            Vector3 velocity;


            if (!grounded)
            {
                velocity = CalculateVelocity() * moveSpeed * (walkSpeedMultiplier * airSpeedMultiplier);
            }
            else if (OnSlope())
            {
                if (IsFloor(_hit.normal))
                {
                    velocity = CalculateSlopeVelocity() * moveSpeed * GetToggleSpeed() * slopeSpeedMultiplier;
                }
                else
                {
                    //Wall
                    velocity = CalculateSlopeVelocity() * moveSpeed * (walkSpeedMultiplier * wallSpeedMultiplier);
                }
            }
            else
            {
                velocity = CalculateVelocity() * moveSpeed * GetToggleSpeed();
                if (vertical < 0)
                {
                    velocity *= moveBackwardFactor;
                    //Inverse Walk backward
                    horizontal = -horizontal;
                }
            }

            rb.AddForce(velocity, ForceMode.Acceleration);
        }

        private float GetToggleSpeed()
        {
            return isSprinting ? walkSpeedMultiplier * sprintSpeedMultiplier : walkSpeedMultiplier;
        }

        private Vector3 CalculateVelocity()
        {
            return (_forwardDirection * vertical + transform.right * horizontal).normalized;
        }

        private void UpdateForwardDirection()
        {
            if (!_cam)
            {
                _cam = Camera.main.transform;
                _forwardDirection = transform.forward;
                return;
            }

            _forwardDirection = _cam.forward;
            _forwardDirection.y = 0;
        }

        private Vector3 CalculateSlopeVelocity()
        {
            return Vector3.ProjectOnPlane(CalculateVelocity(), _hit.normal);
        }

        private bool OnSlope()
        {
            if (!Physics.Raycast(transform.position, Vector3.down, out _hit, 1f, whatIsGround)) return false;
            return _hit.normal != Vector3.up;
        }

        #endregion

        #region SMASH

        private void Smash()
        {
            if (!useSmash) return;
            if (grounded) return;
            jumpLevel = EJumpLevel.LEVEL1;
            rb.AddForce(Vector3.down * smashForce * smashForceMultiplier, ForceMode.Impulse);
        }

        #endregion

        #region DASH

        private float _lastDashTimer;

        private void CheckCanDash()
        {
            if (canDashing || !grounded || Time.time < _lastDashTimer) return;
            _lastDashTimer = Time.time + dashDelay;
            canDashing = true;
        }

        private void Dash()
        {
            if (!useDash) return;
            if (!canDashing) return;
            canDashing = false;
            Vector3 velocity = rb.velocity.normalized;
            float dashMultiplier = grounded ? dashForceMultiplier : airDashForceMultiplier;
            Vector3 dashVelocity = grounded ? groundDashDirection : airDashDirection;
            rb.AddForce((velocity + dashVelocity).normalized * dashMultiplier, ForceMode.Impulse);
            OnDash?.Invoke();
        }

        #endregion

        #region WALL HTTING

        public bool isKnockOut;

        private void CheckKnockOut()
        {
            if (!useKnockout) return;
            //Wall Hitting
            if (rb.velocity.magnitude < knockOutForce || isKnockOut || grounded || canDashing || OnSlope()) return;
            Ray ray = new Ray(transform.position + new Vector3(0, 1f), transform.forward);
            if (Physics.Raycast(ray, out var hit, knockOutCheckDistance, whatIsGround))
            {
                Vector3 knockBackVelocity = Vector3.Reflect(rb.velocity.normalized, hit.normal);

                //Reset velocity
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;

                //Apply knockback
                rb.velocity = knockBackVelocity * knockBackForceMultiplier;
                rb.drag = knockOutDrag;

                vertical = 0;
                horizontal = 0;
                grounded = true;

                isKnockOut = true;
                Invoke(nameof(ResetKnockOut), knockOutDuration);
                OnKnockout?.Invoke();
                enabled = false;
            }
        }

        private void ResetKnockOut()
        {
            isKnockOut = false;
            enabled = true;
        }

        #endregion

        #region JUMP

        public EJumpLevel jumpLevel = EJumpLevel.LEVEL1;
        private float jumpMultiplier;
        private float _lastJump;
        private bool _forceFirstLevelJump = true;

        private void Jump()
        {
            if (!grounded) return;

            //Jump Level
            if (Time.time > _lastJump || _forceFirstLevelJump)
            {
                jumpMultiplier = level1Multiplier;
                jumpLevel = EJumpLevel.LEVEL2;
            }
            else if (jumpLevel == EJumpLevel.LEVEL1)
            {
                jumpMultiplier = level1Multiplier;
                jumpLevel = EJumpLevel.LEVEL2;
            }
            else if (jumpLevel == EJumpLevel.LEVEL2)
            {
                jumpMultiplier = level2Multiplier;
                jumpLevel = EJumpLevel.LEVEL3;
            }
            else if (jumpLevel == EJumpLevel.LEVEL3)
            {
                jumpMultiplier = level3Multiplier;
                jumpLevel = EJumpLevel.LEVEL1;
            }

            _lastJump = Time.time + jumpLevelResetDelay;
            rb.AddForce(Vector3.up * jumpForce * jumpMultiplier, ForceMode.Impulse);

            OnJump?.Invoke();
        }

        #endregion

        private void DragControl()
        {
            rb.drag = grounded ? groundDrag : airDrag;
        }

        private bool _tempGrounded;

        private void CheckGround()
        {
            grounded = Physics.CheckSphere(transform.position, groundCheck, whatIsGround);

            if (grounded != _tempGrounded)
            {
                _tempGrounded = grounded;
                OnGroundedChanged?.Invoke(grounded);
            }
        }

        private bool IsFloor(Vector3 normal)
        {
            return !(Vector3.Angle(Vector3.up, normal) > floorAngle);
        }
    }
}