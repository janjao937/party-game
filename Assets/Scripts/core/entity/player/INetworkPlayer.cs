﻿namespace core.entity.player
{
    public interface INetworkPlayer
    {
        int GetConnectionId();
        void SetConnectionId(int connectionId);
    }
}