﻿namespace core.interact
{
    public interface IInteractable
    {
        void Interact();
    }
}