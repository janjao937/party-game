﻿using Mirror;
using ui.celebrating;
using UnityEngine;

namespace networking.setup
{
    public class CelebratingManager : NetworkBehaviour
    {
        [SerializeField] private int celebratingDuration;
        [Header("UI")] [SerializeField] private CelebratingUI celebratingUI;

        [SyncVar(hook = nameof(OnAnnouncedWinnerEvent))]
        private string winner;

        public override void OnStartServer()
        {
            base.OnStartServer();
            winner = MyNetworkManager.Instance.Winner;
            Invoke(nameof(ReturnToLobby), celebratingDuration);
        }

        [Server]
        private void ReturnToLobby()
        {
            MyNetworkManager.Instance.LoadLobby();
        }

        private void OnAnnouncedWinnerEvent(string old, string newValue)
        {
            celebratingUI.SetWinner(newValue);
        }
    }
}