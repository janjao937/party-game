﻿using System;
using assets;
using Mirror;
using ui.loadingscreen;
using UnityEngine;

namespace networking.setup
{
    public class GameDataManager : NetworkBehaviour
    {
        public static GameDataManager Instance { get; private set; }

        [Serializable]
        public struct MapData
        {
            public bool isStartLoading;
            public int loadSceneDelay;
            public int mapIndex;
            public LoadingScreenType loadingScreenType;
        }

        [SyncVar(hook = nameof(OnMapChangedEvent))]
        public MapData mapData;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }


        private void OnMapChangedEvent(MapData old, MapData currentMapData)
        {
            switch (currentMapData.loadingScreenType)
            {
                case LoadingScreenType.GAMEMAP:
                    MapAssetSO.GameMap gameMap =
                        GameAssetInstance.Instance.MapAssetSo.GetGameMap(currentMapData.mapIndex);
                    LoadingScreenInstance.Instance.LoadingGameMap(currentMapData.isStartLoading,
                        currentMapData.loadSceneDelay,
                        gameMap);
                    break;
                case LoadingScreenType.CELEBRATING:
                    LoadingScreenInstance.Instance.LoadWinner(currentMapData.isStartLoading,
                        currentMapData.loadSceneDelay);
                    break;
                default:
                    Debug.LogWarning("Invalid loading screen type.");
                    break;
            }
        }
    }
}