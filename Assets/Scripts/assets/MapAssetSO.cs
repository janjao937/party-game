﻿using System;
using UnityEngine;

namespace assets
{
    [CreateAssetMenu(fileName = "Map SO", menuName = "Map SO", order = 0)]
    public class MapAssetSO : ScriptableObject
    {
        [Serializable]
        public struct GameMap
        {
            public string sceneName;
            public string mapName;
            public string description;
            public Sprite mapPreview;
        }

        [SerializeField] private GameMap[] gameMaps;

        public GameMap GetGameMap(int id)
        {
            return gameMaps[id];
        }

        public int MapCount => gameMaps.Length;
    }
}