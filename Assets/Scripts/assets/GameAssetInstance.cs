﻿using Singleton;
using UnityEngine;

namespace assets
{
    public class GameAssetInstance : ResourceSingleton<GameAssetInstance>
    {
        [SerializeField] private ItemAssetSO itemAssetSo;
        [SerializeField] private MapAssetSO mapAssetSo;
        public ItemAssetSO ItemAssets => itemAssetSo;
        public MapAssetSO MapAssetSo => mapAssetSo;
    }
}