﻿using System;
using System.Collections.Generic;
using scriptsableobject;
using Singleton;
using UnityEngine;

namespace fx
{
    public class FXInstance : ResourceSingleton<FXInstance>
    {
        [SerializeField] private int initParticle;
        [SerializeField] private CustomParticleData[] customParticleData;

        public override void Awake()
        {
            base.Awake();
            InitFXPool();
        }

        private Dictionary<EnumValue, FXPool> _fxPools;

        private void InitFXPool()
        {
            _fxPools = new Dictionary<EnumValue, FXPool>();
            foreach (var data in customParticleData)
            {
                if (_fxPools.ContainsKey(data.fxParticle.ParticleType)) continue;
                var pool = data.fxPool;
                pool.Init(data.fxParticle);
                pool.Prewarm(initParticle);
                _fxPools.Add(data.fxParticle.ParticleType, pool);
            }
        }

        [Serializable]
        public struct CustomParticleData
        {
            public FXParticle fxParticle;
            public FXPool fxPool;
        }

        public static FXParticle PlayEffect(EnumValue particleType, Vector3 location, Quaternion rotation)
        {
            var pool = Instance._fxPools[particleType];
            var particle = pool.Request();
            pool.Return(particle);
            particle.transform.position = location;
            particle.transform.rotation = rotation;
            particle.gameObject.SetActive(true);
            return particle;
        }
    }
}