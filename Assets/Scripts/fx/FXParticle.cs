﻿using scriptsableobject;
using UnityEngine;

namespace fx
{
    public class FXParticle : MonoBehaviour
    {
        [SerializeField] private EnumValue particleType;
        public EnumValue ParticleType => particleType;
    }
}