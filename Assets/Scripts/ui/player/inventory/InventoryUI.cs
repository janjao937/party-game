﻿using core.inventory.item;
using DG.Tweening;
using UnityEngine;

namespace ui.player.inventory
{
    public class InventoryUI : MonoBehaviour, IVisible
    {
        [Header("ANIMATION")] [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private float hideDelay;
        [SerializeField] private float hideLength;
        [Header("SLOT")] [SerializeField] private ItemSlot[] slots;

        public void SetItemSlot(ItemStack itemStack, int slot)
        {
            slots[slot].SetItem(itemStack);

            DOTween.Kill(canvasGroup);
            canvasGroup.alpha = 1;
            canvasGroup.DOFade(1, hideDelay).OnComplete(() => { canvasGroup.DOFade(0, hideLength); });
        }

        public void SetHeldSlot(int slot)
        {
            foreach (var slotItem in slots)
            {
                slotItem.SetIndicator(false);
            }

            slots[slot].SetIndicator(true);
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}