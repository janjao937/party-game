﻿using assets;
using core.inventory.item;
using UnityEngine;
using UnityEngine.UI;

namespace ui.player.inventory
{
    public class ItemSlot : MonoBehaviour
    {
        [SerializeField] private Image itemImage;
        [SerializeField] private Transform indicator;

        public void SetItem(ItemStack itemStack)
        {
            itemImage.sprite = GameAssetInstance.Instance.ItemAssets.GetItemAsset(itemStack.material).icon;
        }

        public void SetIndicator(bool visible)
        {
            indicator.gameObject.SetActive(visible);
        }
    }
}