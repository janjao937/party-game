﻿using System;
using core.entity.player;
using UnityEngine;

namespace ui.player.debug
{
    public class PlayerDebug : MonoBehaviour
    {
        private PlayerMovement playerMovement;
        private bool _showDebug;

        private void Awake()
        {
            playerMovement = GetComponent<PlayerMovement>();
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                _showDebug = !_showDebug;
            }
        }

        private void OnGUI()
        {
            if (!_showDebug) return;
            if (Event.current.keyCode == KeyCode.Tab || Event.current.character == '\t')
                Event.current.Use();

            GUI.TextArea(new Rect(Screen.width - 200, 10, 200, 300),
                "-------------------------" +
                "\nDEBUG: [Press 'M' to toggle]" +
                "\n-------------------------" +
                $"\ngrounded: {playerMovement.grounded}" +
                $"\ncanDashing: {playerMovement.canDashing}" +
                $"\nisKnockOut: {playerMovement.isKnockOut}" +
                $"\nisSprinting: {playerMovement.isSprinting}" +
                $"\nvertical: {playerMovement.vertical}" +
                $"\nhorizontal: {playerMovement.horizontal}" +
                $"\njumpLevel: {playerMovement.jumpLevel}" +
                "\n-------------------------" +
                "\nCONTROLS:" +
                "\n-------------------------" +
                "\nW,S: Acceleration" +
                "\nA,D: Turn" +
                "\nSpacbar: Jump Lvl.1,2,3" +
                "\nC: Force down" +
                "\nLeft-Shift: Toggle sprint" +
                "\nLeft-Ctrl: Force Lvl. 1 Jump" +
                "\n-------------------------");
        }
    }
}