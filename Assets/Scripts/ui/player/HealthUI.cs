﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ui.player
{
    public class HealthUI : MonoBehaviour, IVisible
    {
        [Header("HEALTH")]
        [SerializeField] private Image healthBar;
        [SerializeField] private Color maxHealthColor = Color.green;
        [SerializeField] private Color lowHealthColor = Color.red;
        [Header("AVATAR")]
        [SerializeField] private Image avatarBar;
        [SerializeField] private Color maxAvatarColor = Color.white;
        [SerializeField] private Color lowAvatarColor = Color.red;

        private void Start()
        {
            SetHealth(100, 100);
        }

        public void SetHealth(float health, float maxHealth)
        {
            float healthScale = Mathf.Clamp01(health / maxHealth);
            UpdateHealthBar(healthScale);
        }
    
        private  void UpdateHealthBar(float value){
    
            float v = Mathf.Clamp01(value);
            healthBar.fillAmount = v;
            healthBar.color = Color.Lerp(lowHealthColor,maxHealthColor, v);
            avatarBar.color = Color.Lerp(lowAvatarColor,maxAvatarColor, v);
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}