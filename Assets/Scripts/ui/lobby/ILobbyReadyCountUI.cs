﻿namespace ui.lobby
{
    public interface ILobbyReadyCountUI: IVisible
    {
        void SetLobbyReadyCount(int ready, int minimumPlayer);
    }
}