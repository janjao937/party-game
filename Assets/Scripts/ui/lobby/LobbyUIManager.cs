﻿using game;
using Mirror;

namespace ui.lobby
{
    public class LobbyUIManager : NetworkBehaviour
    {
        private ILobbyPlayerCountUI _lobbyPlayerCountUI;
        private ILobbyReadyCountUI _lobbyReadyCountUI;

        private void Awake()
        {
            _lobbyPlayerCountUI = GetComponentInChildren<ILobbyPlayerCountUI>();
            _lobbyPlayerCountUI.Visible = false;

            _lobbyReadyCountUI = GetComponentInChildren<ILobbyReadyCountUI>();
            _lobbyReadyCountUI.Visible = false;
        }

        private void Start()
        {
            var gameLobby = FindObjectOfType<GameLobby>();
            gameLobby.OnPlayerCountChanged += OnPlayerCountChangedEvent;
            gameLobby.OnReadyCountChanged += OnReadyCountChangedEvent;
        }

        private void OnPlayerCountChangedEvent(int currentPlayer, int maxPlayer)
        {
            _lobbyPlayerCountUI.SetLobbyPlayerCount(currentPlayer, maxPlayer);
            _lobbyPlayerCountUI.Visible = true;
        }

        private void OnReadyCountChangedEvent(int readyPlayer, int minimumPlayer)
        {
            _lobbyReadyCountUI.SetLobbyReadyCount(readyPlayer, minimumPlayer);
            _lobbyReadyCountUI.Visible = true;
        }
    }
}