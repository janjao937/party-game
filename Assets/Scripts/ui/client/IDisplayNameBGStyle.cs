﻿namespace ui.client
{
    public interface IDisplayNameBgStyle
    {
        public enum DisplayNameBgStyle
        {
            BLUE,
            YELLOW,
            RED,
            GREEN,
        }

        DisplayNameBgStyle GetStyle();
        void SetStyle(DisplayNameBgStyle displayNameBgStyle);
    }
}