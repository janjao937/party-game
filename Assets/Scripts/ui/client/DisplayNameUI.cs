﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ui.client
{
    public class DisplayNameUI : MonoBehaviour, IDisplayNameBgStyle, IVisible
    {
        [SerializeField] private TextMeshProUGUI displayNameText;
        [SerializeField] private Image bg;
        [SerializeField] private Sprite blueBg;
        [SerializeField] private Sprite yellowBg;
        [SerializeField] private Sprite redBg;
        [SerializeField] private Sprite greenBg;

        private IDisplayNameBgStyle.DisplayNameBgStyle _displayNameBgStyle;

        public void SetDisplayName(string displayName)
        {
            displayNameText.text = displayName;
        }

        public IDisplayNameBgStyle.DisplayNameBgStyle GetStyle() => _displayNameBgStyle;

        public void SetStyle(IDisplayNameBgStyle.DisplayNameBgStyle displayNameBgStyle)
        {
            _displayNameBgStyle = displayNameBgStyle;
            switch (displayNameBgStyle)
            {
                case IDisplayNameBgStyle.DisplayNameBgStyle.BLUE:
                    bg.sprite = blueBg;
                    break;
                case IDisplayNameBgStyle.DisplayNameBgStyle.YELLOW:
                    bg.sprite = yellowBg;
                    break;
                case IDisplayNameBgStyle.DisplayNameBgStyle.RED:
                    bg.sprite = redBg;
                    break;
                case IDisplayNameBgStyle.DisplayNameBgStyle.GREEN:
                    bg.sprite = greenBg;
                    break;
            }
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}