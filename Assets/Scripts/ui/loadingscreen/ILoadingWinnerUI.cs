﻿namespace ui.loadingscreen
{
    public interface ILoadingWinnerUI : IVisible
    {
        void LoadingWinner(bool isLoadingStart, float loadSceneDelay);
    }
}