﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ui.loadingscreen
{
    public class LoadingWinnerUI : MonoBehaviour, ILoadingWinnerUI
    {
        [Header("FADE")] [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Image fadeImage;
        [Range(0, 1f)] [SerializeField] private float fadeFactor;

        private void Awake()
        {
            Visible = false;
        }

        public void LoadingWinner(bool isLoadingStart, float loadSceneDelay)
        {
            loadSceneDelay *= 0.5f;

            Visible = true;

            //Fade
            float previewDuration = loadSceneDelay * fadeFactor;
            float fadeDuration = loadSceneDelay - previewDuration;

            float startValue = isLoadingStart ? 0 : 1;
            canvasGroup.alpha = startValue;

            Color startColor = fadeImage.color;
            startColor.a = startValue;
            fadeImage.color = startColor;

            if (isLoadingStart)
            {
                canvasGroup.DOFade(1, fadeDuration).OnComplete(() => { fadeImage.DOFade(1, fadeDuration); });
            }
            else
            {
                transform.DOScale(Vector3.one, previewDuration)
                    .OnComplete(() =>
                    {
                        fadeImage.DOFade(0, fadeDuration).OnComplete(() =>
                        {
                            canvasGroup.DOFade(0, fadeDuration).OnComplete(() => { Visible = false; });
                        });
                    });
            }
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}