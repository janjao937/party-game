﻿using assets;

namespace ui.loadingscreen
{
    public interface ILoadingScreenUI : IVisible
    {
        void LoadingScreen(bool isLoadingStart, float loadSceneDelay, MapAssetSO.GameMap gameMap);
    }
}