﻿using assets;
using Singleton;

namespace ui.loadingscreen
{
    public class LoadingScreenInstance : ResourceSingleton<LoadingScreenInstance>
    {
        private ILoadingScreenUI _loadingScreenUI;
        private ILoadingWinnerUI _loadingWinnerUI;

        public override void Awake()
        {
            base.Awake();
            _loadingScreenUI = GetComponentInChildren<ILoadingScreenUI>();
            _loadingWinnerUI = GetComponentInChildren<ILoadingWinnerUI>();
        }

        public void LoadingGameMap(bool isLoadingIn, int loadingDelay, MapAssetSO.GameMap gameMap)
        {
            _loadingScreenUI.LoadingScreen(isLoadingIn, loadingDelay, gameMap);
        }

        public void LoadWinner(bool isLoadingIn, int loadingDelay)
        {
            _loadingWinnerUI.LoadingWinner(isLoadingIn, loadingDelay);
        }
    }
}