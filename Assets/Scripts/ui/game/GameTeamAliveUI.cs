﻿using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GameTeamAliveUI : MonoBehaviour, IGameTeamAliveUI
    {
        [SerializeField] private TextMeshProUGUI goodTeamText;
        [SerializeField] private TextMeshProUGUI badTeamText;

        public void SetTeam(GameUIManager.TeamPlayerData teamPlayerData)
        {
            goodTeamText.text = $"{teamPlayerData.good}";
            badTeamText.text = $"{teamPlayerData.bad}";
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}