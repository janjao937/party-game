﻿using DG.Tweening;
using scriptsableobject;
using sfx;
using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GameScoreUI : MonoBehaviour, IGameScoreUI
    {
        [Header("SFX")] [SerializeField] private EnumValue scoreSfx;
        [Header("UI")] [SerializeField] private TextMeshProUGUI headerText;
        [SerializeField] private TextMeshProUGUI scoreText;

        private int _lastScore;
        public void SetScore(GameUIManager.PlayerScoreData playerScoreData)
        {
            if (_lastScore == playerScoreData.score)
            {
                scoreText.color = playerScoreData.color;
                return;
            }
            
            _lastScore = playerScoreData.score;
            headerText.text = playerScoreData.header;
            scoreText.text = $"{playerScoreData.score}";
            scoreText.color = playerScoreData.color;

            RectTransform rectTransform = scoreText.GetComponent<RectTransform>();
            rectTransform.localScale = Vector3.one;

            DOTween.Kill(rectTransform);
            rectTransform.DOPunchScale(Vector3.one * 0.25f, 0.23f);
            
            SoundInstance.Instance.PlaySound(scoreSfx);
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}