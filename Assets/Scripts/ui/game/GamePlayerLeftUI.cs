﻿using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GamePlayerLeftUI : MonoBehaviour, IGamePlayerLeftUI
    {
        [SerializeField] private TextMeshProUGUI infoText;

        public void SetPlayerLeft(int playerLeft)
        {
            if (playerLeft > 1)
            {
                infoText.text = $"{playerLeft} players left!";
            }
            else
            {
                infoText.text = $"{playerLeft} player left!";
            }
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}