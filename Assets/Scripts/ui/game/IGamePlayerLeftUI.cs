﻿namespace ui.game
{
    public interface IGamePlayerLeftUI : IVisible
    {
        void SetPlayerLeft(int playerLeft);
    }
}