﻿namespace ui.game
{
    public interface IGameInfoUI : IVisible
    {
        void SetHeader(string header);
        void SetInfo(string info);
    }
}