﻿namespace ui.game
{
    public interface IGameTimerUI: IVisible
    {
        void SetTimer(int time);
    }
}