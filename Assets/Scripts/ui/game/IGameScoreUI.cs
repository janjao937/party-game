﻿namespace ui.game
{
    public interface IGameScoreUI: IVisible
    {
        void SetScore(GameUIManager.PlayerScoreData playerScoreData);
    }
}