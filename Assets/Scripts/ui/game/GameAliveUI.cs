﻿using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GameAliveUI : MonoBehaviour, IGameAliveUI
    {
        [SerializeField] private TextMeshProUGUI aliveText;

        public void SetAlive(int alive)
        {
            aliveText.text = $"{alive}";
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}